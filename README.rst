PoB Runtime
===========

A cross platform runtime for `PathOfBuilding <https://github.com/PathOfBuildingCommunity/PathOfBuilding>`_.

The runtime is already cross platform so this is mostly a way to ease its compliation and allow to easily change the lua implementation to use.

Credits
-------

The source files included in the repository are comming from archvies distributed by the original author from PathOfBuilding, David Gowor

* `PathOfBuilding-runtime-src.zip <https://github.com/Openarl/PathOfBuilding/files/1167199/PathOfBuilding-runtime-src.zip>`_
* `runtime-win32.zip <https://github.com/Openarl/PathOfBuilding/blob/master/runtime-win32.zip>`_

Dependencies
------------

* cURL
* zlib
* lua (either lua51 or luajit)

Build
-----

You'll need cmake on top of a C and C++ compiler.

Do mind that you need to retrieve the submodules when cloning the repository.

Then run::

  $ mkdir build
  $ cd build
  $ cmake .. -DCMAKE_INSTALL_PREFIX=some/path -DLUA_IMPL=luajit
  $ cmake --build .
  $ cmake --install .

This will install the runtime to some/path/lua. So some/path is expected to point to a PathOfBuilding installation.
